import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})


export class DataService {
	
	private NUMBER_ROWS   : number;
	private NUMBER_COLUMNS: number;

	constructor() {
		this.NUMBER_ROWS    = 200;
		this.NUMBER_COLUMNS = 200;
	}
	
	create_grid(): number[][] {
		let grid = [];
		
		for (let i=0; i<this.NUMBER_ROWS; i++) {
			grid.push([])
			
			for (let j=0; j<this.NUMBER_COLUMNS; j++) {
				grid[i][j] = (i+1).toString() + " - " + (j+1).toString();
			}
		}
		
		return grid;
	}
}
