import { Injectable  } from '@angular/core';
import { DataService } from './data.service';


@Injectable({
  providedIn: 'root'
})


export class LazyService {
	
	public data        : number[][];
	public visibleCells: number[][];
	private visibleRow   : number;
	private visibleColumn: number;

	constructor(
		private _dataService: DataService
	) {
		this.data = undefined;
		this.visibleRow    = 25;
		this.visibleColumn = 25;
	}
	
	initService(): void {
		this.getData();
	}
	
	getVisibleCells(): void {
		let visibleCells = this.data.slice(this.visibleRow-25, this.visibleRow+25);
		visibleCells = visibleCells.map(row => row.slice(this.visibleColumn-25, this.visibleColumn+25));
		this.visibleCells = visibleCells;
		console.log(this.visibleCells);
	}
	
	getData(): void {
		let data = this._dataService.create_grid();
		this.data = data;
		this.getVisibleCells();
	}
	
	setVisibleRow(row: number): void {
		if (this.visibleRow != row) {
			this.visibleRow = row;
			console.log(this.visibleRow);
			this.getVisibleCells();
		}
	}
	
	setVisibleColumn(column: number): void {
		if (this.visibleColumn != column) {
			this.visibleColumn = column;
			console.log(this.visibleColumn);
			this.getVisibleCells();
		}
	}
 
}
