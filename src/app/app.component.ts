import { Component, OnInit } from '@angular/core';
import { LazyService } from '../services/lazy.service';


@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.scss']
})


export class AppComponent implements OnInit {
	
	public visibleCells: number[][];
	
	constructor(
		public _lazyService: LazyService
	) {
		this.visibleCells = this._lazyService.data;
	}
	
	ngOnInit() {
		this._lazyService.initService();
		//this.visibleCells = this._lazyService.getVisibleCells();
		//console.log(this.visibleCells);
	}
 	
}
