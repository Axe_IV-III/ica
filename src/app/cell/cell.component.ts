import { Component, OnInit, Input, Renderer2 } from '@angular/core';
import { LazyService } from '../../services/lazy.service';


@Component({
  selector   : 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls  : ['./cell.component.scss']
})


export class CellComponent implements OnInit {
	
	@Input() content: string;
	public row   : number;
	public column: number;

	constructor(
		private _lazyService: LazyService,
		private renderer: Renderer2
	) {}

	ngOnInit(): void {
		this.row    = +this.content.split(' - ')[0];
		this.column = +this.content.split(' - ')[1];
	}
	
	public onView({target, visible}: {target: Element; visible: boolean}): void {
		if (visible) {
			if      (this.row%25==0)    this._lazyService.setVisibleRow(this.row);
			else if (this.column%25==0) this._lazyService.setVisibleColumn(this.column);
		}
    }

}
